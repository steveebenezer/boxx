# BOXX.AI website source code

------------------------

## Requirements

- [Yarn](http://yarnpkg.com)
- [Parcel](parceljs.org)

## Instructions/setup

- Development server `yarn start`
- Build source `yarn build`
