import "./styles/index.scss";

import "./scripts/vendors/jquery.js";
import "./scripts/vendors/owlcarousel.js";
import "./scripts/vendors/scrollify.js";

import "./scripts/main.js";